\documentclass[usenames,dvipsnames]{beamer}
   
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{DejaVuSansMono}
\usepackage[sfdefault,lf]{FiraSans}
%\usepackage[scaled=0.85]{FiraMono}
\usepackage{minted}
\usepackage{array}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}

\usetheme{metropolis}
\usemintedstyle{trac}
\newcommand{\rust}[1]{\mintinline{rust}{#1}}
\graphicspath{{images/}}
\title{Programación de sistemas con Rust}
\author{Alberto Carmona y Sergio Cayuela}
\institute{Universidad de Málaga}
\date{Enero 2020}
\subject{Algoritmia y Complejidad}
\begin{document}
 
\frame{\titlepage}
 
\begin{frame}{Introducción}
    \begin{itemize}
        \item Lenguaje multiparadigma de uso general
        \item Objetivo: programación de sistemas segura pero ágil
        \item Soporte a la concurrencia
        \item Concebido por Graydon Hoare en 2006 (Mozilla)
    \end{itemize}
\end{frame}
 
\begin{frame}{¡Hola, mundo!}
\begin{figure}[H]
    \centering
    \inputminted{rust}{code/helloworld.rs}
    \begin{itemize}
        \item La función \rust{main()} se ejecuta primero
        \item \rust{println!} es un \textbf{macro}, no una función
        \item \rust{"Hello, world!"} es un \textit{string slice}, no un \rust{String}
        \item Punto y coma y llaves obligatorias
    \end{itemize}
\end{figure}
\end{frame}
\begin{frame}{La cadena de herramientas}
    \begin{itemize}
        \item \rust{rustup}: Instalación, actualización, gestión de las \textit{toolchains}
        \item \rust{rustc}: Compilador. 
        \item \rust{cargo}: Gestor de dependencias, \textit{builder}, analización y ejecución de proyectos.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]{\rust{rustc}: Sistema de lints}
    \begin{itemize}
        \item Varios niveles: \textit{allow, warn, deny, forbid}
    \end{itemize}
    \rust{fn foo() {}}
\begin{verbatim}
warning: function is never used: `foo`
 --> src/lib.rs:2:1
  |
2 | fn foo() {}
  | ^^^^^^^^
  |
\end{verbatim}
\end{frame}
\begin{frame}{Variables}
    \begin{itemize}
        \item Se declaran con \rust{let}
        \item Estáticamente tipado
        \item Inferencia de tipos
    \end{itemize}
\end{frame}
\begin{frame}{Variables: inmutables por defecto}
\begin{figure}[H]
    \inputminted{rust}{code/immutable.rs}
    \caption{Esto dará un error de compilación: la variable x es inmutable}
\end{figure}
\end{frame}
\begin{frame}{Variables: ocultación}
    \begin{figure}[H]
        \inputminted{rust}{code/shadowing.rs}
        \caption{Este código es correcto en Rust. La variable tomará el último valor que se le da}
    \end{figure}
\end{frame}
\begin{frame}{Variables mutables}
    \begin{figure}[H]
        \inputminted{rust}{code/mutable.rs}
        \caption{Este código es válido, ya que la variable es mutable}
    \end{figure}
\end{frame}
\begin{frame}{Constantes}
    \begin{itemize}
        \item Siempre inmutables y anotadas
    \end{itemize}
    \begin{figure}[H]
        \inputminted{rust}{code/const.rs}
    \end{figure}
\end{frame}
\begin{frame}{Datos primitivos escalares}
    \begin{itemize}
        \item \textbf{Enteros}: de 8, 16, 32, 62 y 128 bits, con signo (\rust{i32}, \rust{i64}...) y sin signo (\rust{u32}, \rust{u64}...)
        \item \textbf{Números de punto flotante}: \rust{f32}, \rust{f64}
        \item \textbf{Booleanos} (\rust{bool})
        \item \textbf{Caracteres}: Valor escalar Unicode
    \end{itemize}
\end{frame}
\begin{frame}{Datos primitivos compuestos}
    \begin{itemize}
        \item \textbf{Tuplas}: Lista de valores mixta \rust{let tup: (i32, f64, u8) = (500, 6.4, 1);}
        \item \textbf{Arrays}: Lista de valores no mixta
    \end{itemize}
\end{frame}
\begin{frame}{Arrays seguros}
        \begin{figure}[h]
            \inputminted[fontsize=\small]{rust}{code/unsafe_array.rs}
            \centering
            \caption{Este programa dará un fallo en tiempo de ejecución}
        \end{figure}
\end{frame}
\begin{frame}{Funciones}
    \begin{itemize}
        \item Construcción \rust{fn}
        \item Pueden devolver un valor (en cuyo caso deberán estar anotadas) o no 
    \end{itemize}
\end{frame}
\begin{frame}{Expresiones, sentencias y bloques}
\begin{figure}[H]
    \inputminted[fontsize=\small, baselinestretch=0.7]{rust}{code/functions.rs}
    \caption{La función \rust{plus_one} toma un parámetro y devuelve una expresión. El bloque dentro de \rust{let y} también se evalúa a una expresión.}
\end{figure}
\end{frame}
\begin{frame}{Control de flujo}
    \begin{itemize}
        \item Las construcciones usuales: \texttt{if}, \texttt{if-else}, \texttt{while}
        \item \texttt{for} al estilo Python
        \begin{figure}[H]
            \inputminted{rust}{code/for.rs}
            \caption{\rust{(1..4)} va del 1 al 4 exclusive, y \rust{rev()} lo invierte}
        \end{figure}
    \end{itemize}
\end{frame}
\begin{frame}{Control de flujo}
    \begin{itemize}
        \item \rust{loop}: bucle infinito
        \item \rust{break}: puede devolver un valor
        \begin{figure}[H]
            \inputminted[fontsize=\small, baselinestretch=0.7]{rust}{code/loop.rs}
            \caption{El resultado del bloque \rust{loop} se asigna a la variable mutable \rust{counter}}
        \end{figure}
    \end{itemize}
\end{frame}
\begin{frame}{Propiedad (\textit{Ownership})}
    \begin{itemize}
        \item Mecanismo para la gestión del montón (\textit{heap})
        \item La variable es propietaria del valor que almacena
        \item Cuando una variable se \textbf{invalida}, la memoria se libera
    \end{itemize}
\end{frame}
\begin{frame}{Propiedad (\textit{Ownership})}
    \begin{figure}[H]
        \inputminted[fontsize=\small]{rust}{code/scope.rs}
        \caption{Cuando la variable deja de ser válida, \textit{drop} hace su trabajo: libera la memoria asociada.}
    \end{figure}
\end{frame}
\begin{frame}{Propiedad (\textit{Ownership})}
    \begin{figure}[H]
        \inputminted[fontsize=\tiny]{rust}{code/functionscope.rs}
        \caption{Tras llamar a \rust{takes_ownership(s)}, \rust{s} es liberada: ya no la podemos usar.}
    \end{figure}
\end{frame}
\begin{frame}{Propiedad (\textit{Ownership})}
    \begin{figure}[H]
        \inputminted[fontsize=\tiny]{rust}{code/functionscope2.rs}
    \end{figure}
\end{frame}
\begin{frame}{Referencias y préstamos}
\begin{figure}[H]
    \inputminted[fontsize=\small]{rust}{code/references.rs}
    \caption{Como le estamos pasando una referencia, Rust no libera la memoria asociada a \rust{s} hasta que no sale de \rust{main()}}
\end{figure}
\end{frame}
\begin{frame}{Problemas de programación de sistemas: \textit{double free}}
    \begin{itemize}
        \item Liberar un espacio de memoria ya liberado implica un comportamiento impredecible
        \item Rust evita esta situación a toda costa
    \end{itemize}
    \begin{figure}[H]
        \inputminted{rust}{code/doublefree.rs}
    \end{figure}
\end{frame}
\begin{frame}{Problemas de programación de sistemas: \textit{double free}}
    \begin{figure}[H]
        \includegraphics[width=200px]{unsafe_pointers.png}
    \end{figure}
\end{frame}
\begin{frame}{Problemas de programación de sistemas: \textit{double free}}
    \begin{figure}[H]
        \includegraphics[width=200px]{safe_pointers.png}
    \end{figure}
\end{frame}
\begin{frame}{Problemas de programación de sistemas: \textit{double free}}
    \begin{figure}[H]
        \includegraphics[width=170px]{cloning.png}
    \end{figure}
\end{frame}
\begin{frame}{Problemas de programación de sistemas: \textit{data race}}
    \begin{figure}[H]
        \inputminted{rust}{code/datarace.rs}
        \caption{Este código dará un error de compilación.}
    \end{figure}
\end{frame}
\begin{frame}{Problemas de programación de sistemas: \textit{dangling pointers}}
    \begin{figure}[H]
        \inputminted{rust}{code/dangle.rs}
        \caption{La variable \rust{s} se crea dentro de \rust{dangle()} y devolvemos una referencia a ella pero, al salir de la función, \rust{s} deja de ser válida y Rust hace un \rust{drop}: ¡referencia colgante! Este programa no compilará.}
    \end{figure}
\end{frame}
\begin{frame}{Tiempos de vida (\textit{lifetimes})}
    \begin{itemize}
        \item Consideramos el caso de una función que toma dos cadenas y devuelve la cadena más larga
    \end{itemize}
    \begin{figure}[H]
        \inputminted[fontsize=\small]{rust}{code/lifetime.rs}
    \end{figure}
\end{frame}
\begin{frame}{Tiempos de vida (\textit{lifetimes})}
\begin{figure}[H]
    \inputminted[fontsize=\footnotesize]{rust}{code/lifetime2.rs}
\end{figure}
\end{frame}
\begin{frame}{Tiempos de vida (\textit{lifetimes})}
\begin{figure}[H]
    \inputminted[fontsize=\small]{rust}{code/lifetime3.rs}
\end{figure}
\end{frame}
\begin{frame}{Punteros inteligentes}
    \begin{itemize}
        \item Son \textit{structs} que implementan los rasgos \rust{Deref} y \rust{Drop}
        \item Los rasgos definen el comportamiento de las funciones, como las interfaces de Java o las clases de Haskell (\texttt{deriving Show})
        \item Implementando \rust{Deref}, nuestros tipos pueden ser desreferenciados
    \end{itemize}
\end{frame}
\begin{frame}{Desreferenciación con \rust{Deref}}
    \begin{figure}[H]
        \inputminted[fontsize=\scriptsize]{rust}{code/deref.rs}
        \caption{Implementando \rust{Deref} para nuestro tipo genérico \rust{MyBox<T>}}
    \end{figure}
\end{frame}
\begin{frame}{Desreferenciación con \rust{Deref}}
\begin{figure}[H]
    \inputminted{rust}{code/deref2.rs}
\end{figure}
\end{frame}
\begin{frame}{Desreferenciación implícita (\textit{deref coercion})}
\begin{figure}[H]
    \inputminted{rust}{code/derefcoercion.rs}
    \caption{Rust convierte nuestro \rust{&MyBox<String>} en \rust{&String} para poder pasárselo a la función \rust{hello()}}
\end{figure}
\end{frame}
\begin{frame}{El puntero inteligente \rust{Box<T>}}
    \begin{itemize}
        \item Sirve para almacenar información en el montón
        \item Un uso común es el de definir un tipo cuyo tamaño no puede ser definido en tiempo de compilación
    \end{itemize}
\end{frame}
\begin{frame}{El puntero inteligente \rust{Box<T>}}
    \begin{figure}[H]
        \inputminted{rust}{code/conslist.rs}
        \caption{\rust{Cons} se contiene a sí mismo. ¡Rust no sabe su tamaño!}
    \end{figure}
\end{frame}
\begin{frame}{El puntero inteligente \rust{Box<T>}}
    \begin{figure}[H]
        \inputminted{rust}{code/conslist.rs}
        \caption{\rust{Cons} se contiene a sí mismo. ¡Rust no sabe su tamaño!}
    \end{figure}
\end{frame}
\begin{frame}{El puntero inteligente \rust{Box<T>}}
    \begin{figure}[H]
        \inputminted[fontsize=\small]{rust}{code/conslistbox.rs}
    \end{figure}
\end{frame}
\begin{frame}{Concurrencia y clausuras}
    \begin{itemize}
        \item La librería estándar de Rust tiene un módulo para programación concurrente: \rust{std::thread}
        \item Mecanismos de creación y manejo de hilos: \rust{spawn}, \rust{sleep}, \rust{join}
        \item \rust{spawn} recibe una \textbf{clausura} como argumento: una función anónima que puede capturar su entorno. Las variables que se definen fuera de la clausura pueden ser utilizadas dentro de ella
    \end{itemize}
\end{frame}
\begin{frame}{Concurrencia y clausuras}
    \begin{figure}[H]
        \inputminted[fontsize=\scriptsize]{rust}{code/threads.rs}
    \end{figure}
\end{frame}
\begin{frame}{Concurrencia y clausuras}
    \begin{figure}[H]
        \inputminted[fontsize=\small]{rust}{code/threads2.rs}
        \caption{Sin \rust{move}, este programa no compilaría}
    \end{figure}
\end{frame}
\begin{frame}{Conclusiones}
    \begin{itemize}
        \item Solo hemos presentado una pequeña parte del lenguaje
        \item Rust ofrece programación orientada a objetos, mecanismos de captura de excepciones, azúcar sintáctico...
        \item Lo que no puedes hacer con Rust es tan importante como lo que puedes hacer con él
    \end{itemize}
\end{frame}
\begin{frame}{Fin}
    \begin{center}
         \huge ¿Alguna pregunta?
    \end{center}
\end{frame}
\end{document}
