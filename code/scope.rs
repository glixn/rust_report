{
    let s = String::from("hello");

    // do stuff with s
}   
// this scope is now over, and s is no
// longer valid
