\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fouriernc} 
\usepackage[scaled=.8]{DejaVuSansMono}
%\usepackage{fontspec}
\usepackage{parskip} % Spacing between sections
\usepackage{microtype} % Paragraph alignment
\usepackage{enumitem} % Itemize vertical space
\usepackage{graphicx}
\usepackage{float} % Absolute figure positioning
\usepackage[format=plain,
            font=it]{caption}
\usepackage[spanish]{babel}
\usepackage[
style=numeric,
sorting=none,
backend=biber
]{biblatex}
\usepackage[pdfencoding=auto,hidelinks,linktoc=all]{hyperref} % Hyperlinks for TOC and bibliography
\usepackage{xurl} % Allows \url to be broken (for a line break) anywhere
\usepackage{minted} % Code highlighting
\usepackage{csquotes} % Required by biblatex

% Config
\usemintedstyle{trac}
\setminted{baselinestretch=0.7}
\setlist{nosep}
%\setmainfont{TeX Gyre Schola}
\graphicspath{{images/}}
\addbibresource{bibliography.bib}
%\newfontfamily\DejaSans{DejaVu Sans}

% Commands
\newcommand{\rust}[1]{\mintinline{rust}{#1}}

\begin{document}
\nocite{*}

\include{title}

\tableofcontents
\pagebreak

\section{Introducción}
Rust es un lenguaje de programación multiparadigma de uso general que surge de la necesidad de sus creadores de diseñar un lenguaje de programación de sistemas seguro, con buen soporte a la concurrencia y a la vez práctico y ágil.

Rust empezó en 2006 como el proyecto personal de Graydon Hoare, trabajador de Mozilla. En 2009, Mozilla empezó a patrocinar Rust y a utilizarlo para desarrollar componentes de su propio navegador web, Firefox. A día de hoy, la empresa sigue patrocinando el proyecto, aunque gran parte del equipo de Rust es ajeno a ella.

Tiene como objetivo ser un lenguaje eficiente a la vez que cómodo, tratando de reconciliar la precisión y el control de un lenguaje de bajo nivel con la facilidad de uso y la accesibilidad. Por ello, es un lenguaje con un claro enfoque a la programación de sistemas, si bien se usa en proyectos muy diversos: desde servidores y frameworks para aplicaciones web hasta motores de videojuegos, pasando por componentes de navegadores web y motores de simulación de realidad virtual.

En las próximas secciones veremos cómo esta fusión de los conceptos clásicos de programación de bajo nivel con el diseño y la abstracción de los lenguajes de programación modernos distingue a Rust de otros lenguajes y lo convierte en una herramienta viable para una gran variedad de proyectos y en una alternativa decente a los lenguajes de programación de sistemas más populares.

\section{Un primer vistazo a la sintaxis}
Para introducir la sintaxis de Rust, se muestra el siguiente programa \guillemotleft {\it ¡Hola, mundo!}\guillemotright:

\begin{figure}[H]
    \inputminted{rust}{code/helloworld.rs}
    \caption{helloworld.rs}
\end{figure}

Varias observaciones sobre este programa:

\begin{itemize}
    \item \rust{fn main()} declara la función \rust{main} que, al igual que en Java o en C, es una función especial: siempre se ejecuta primero. 
    \item Las llaves de apertura y cierre son obligatorias.
    \item \rust{println!} es una macro de Rust, no una función. Todas las macros utilizan el caracter \rust{!} detrás de su nombre. Se diferencian de las funciones en que las macros se substituyen por funciones más complejas al compilar. Son \textit{metacódigo}: código en Rust que genera código de Rust. Rust no admite un número variable de argumentos en las funciones, así que \rust{println!} se implementa como una macro para poder pasarle variables a las cadenas al estilo \texttt{printf} de C: \rust{println!("x is: {}", x);}.
    \item \rust{"Hello, world!"} es una cadena de caracteres literal del tipo primitivo \rust{&str}, distinto del tipo \rust{String} de la librería estándar, del que hablaremos más adelante.
    \item La sentencia termina en un punto y coma, que se usa de forma similar a C o Java. La terminación en punto y coma es obligatoria para las sentencias.
\end{itemize}

En definitiva, podemos afirmar que Rust es un lenguaje imperativo y funcional no puro.

\section{La cadena de herramientas de Rust}
Rust se instala como un conjunto de herramientas. Hablaremos de las tres principales: \rust{rustup}, \rust{rustc} y \rust{cargo}.

\subsection{\texttt{rustup}}
Instala las herramientas mencionadas y se encarga de la actualización y la gestión de las cadenas de herramientas ({\it toolchains}), de las cuales puede haber varias y pueden ser configuradas para compilar para distintas plataformas.

\subsection{\texttt{rustc}}
Es el compilador de Rust. Provee varios niveles de optimización y compila para un gran número de arquitecturas; desde las más populares, como x86 o ARMv8, hasta otras menos usadas, como PowerPC o SPARC64, y los sistemas operativos habituales. \texttt{rustc} permite, además, especificar ciertas extensiones del conjunto de instrucciones si sabemos que el procesador las soporta, como \texttt{BMI} para manipulación de bits o \texttt{AES} para criptografía.

El compilador de Rust ofrece una serie de \textit{lints}. Se trata de un conjunto de reglas clasificadas en distintos niveles (\textit{allow}, \textit{warn}, \textit{deny} y \textit{forbid}) que el compilador comprueba que se cumplen en tiempo de compilación. Según su nivel, en caso de que no se cumplan, el compilador lanzará una advertencia, un error o las dejará pasar. Por ejemplo, \texttt{dead-code} detecta funciones y variables declaradas que no se usan:

\begin{figure}[H]
\rust{fn foo() {}}
\vspace{0.5\baselineskip}
\begin{verbatim}
warning: function is never used: `foo`
 --> src/lib.rs:2:1
  |
2 | fn foo() {}
  | ^^^^^^^^
  |
\end{verbatim}
\caption{\texttt{dead-code} en acción}
\end{figure}

Cabe destacar que se puede cambiar el nivel de cualquier \textit{lint}, además de definir \textit{lints} personalizados.
        
\subsection{\texttt{cargo}}
Es el gestor de dependencias y \textit{builder} de Rust. Proporciona comandos para crear la estructura de un proyecto nuevo, analizarlo para encontrar errores y compilar el proyecto con sus dependencias, opcionalmente ejecutándolo después. Utiliza un fichero de configuración escrito en un lenguaje denominado \textit{TOML} para especificar las dependencias y algunos parámetros sobre el proyecto como el nombre y la versión.

\section{Conceptos habituales}
En esta sección se introducen una serie de conceptos básicos comunes a muchos lenguajes de programación por procedimientos y estructurada.

\subsection{Variables}
En Rust, las variables se declaran con la palabra reservada \rust{let}. Rust es un lenguaje estáticamente tipado, así que debe conocer el tipo de todas las variables en tiempo de compilación, pero también es capaz de inferir el tipo si no está anotado. A continuación veremos los distintos tipos de variables:

\subsubsection{Inmutables}
Las variables en Rust son inmutables por defecto. Esto significa que, una vez se les ha asignado un valor, no se pueden modificar.

\begin{figure}[H]
    \inputminted{rust}{code/immutable.rs}
    \caption{Esto dará un error de compilación: la variable x es inmutable}
\end{figure}

Sin embargo, Rust nos permite \textit{ocultar} la variable. Es decir, podemos volver a asignarle un valor a una variable con la construcción \rust{let}, ocultando su valor antiguo y asignándole uno nuevo, no necesariamente del mismo tipo.

\begin{figure}[H]
    \inputminted{rust}{code/shadowing.rs}
    \caption{Este código es correcto en Rust. La variable tomará el último valor que se le da}
\end{figure}

\subsubsection{Mutables}
Las variables mutables se definen con la construcción \rust{let mut}, y su valor puede ser reasignado. Sin embargo, su tipo es inmutable, y el valor reasignado debe ser del mismo tipo que la variable.

\begin{figure}[H]
    \inputminted{rust}{code/mutable.rs}
    \caption{Este código es válido, ya que la variable es mutable}
\end{figure}

\subsubsection{Constantes}
Las constantes son declaradas con \rust{const} y son siempre inmutables. Además, siempre se debe anotar su tipo:

\begin{figure}[H]
    \inputminted{rust}{code/const.rs}
\end{figure}

\subsection{Tipos de datos primitivos}
Existen dos clases de tipos de datos primitivos en Rust: escalares y compuestos.

\subsubsection{Escalares}
Los siguientes tipos de datos se denominan escalares:

\begin{itemize}
    \item \textbf{Enteros}: Los hay de 8, 16, 32, 62 y 128 bits, con signo (\rust{i32}, \rust{i64}...) y sin signo (\rust{u32}, \rust{u64}...). También hay dos tipos que dependen de la arquitectura para la que se compile: \rust{isize} y \rust{usize}. Las operaciones nativas son las habituales: suma, resta, multiplicación, división y módulo.
    \item \textbf{Números de punto flotante}: De 32 y 64 bits (\rust{f32}, \rust{f64}).
    \item \textbf{Booleanos} (\rust{bool}).
\item \textbf{Caracteres}: Representan un valor escalar Unicode y se escriben entre comillas simples.
\end{itemize}

\subsubsection{Compuestos}
Los siguientes tipos de datos son compuestos:

\begin{itemize}
    \item \textbf{Tuplas}: Es una lista de valores mixta: 

        \rust{let tup: (i32, f64, u8) = (500, 6.4, 1);}
    \item \textbf{Arrays}: Es una lista de valores no mixta y de longitud fija. Una característica destacable es que Rust lanza un error en tiempo de ejecución si se accede a un índice incorrecto:
        
        \begin{figure}[h]
            \inputminted{rust}{code/unsafe_array.rs}
            \centering
            \caption{Este programa dará un fallo en tiempo de ejecución}
        \end{figure}
        
\end{itemize}

\subsection{Funciones y bloques}
Ya hemos visto en el programa \guillemotleft\textit{¡Hola, mundo!}\guillemotright que las funciones se definen con la construcción \rust{fn}. Pueden estar anotadas con un tipo (en cuyo caso deberán devolver un valor) o no. Como es de esperar, pueden tener parámetros, que siempre deberán tener un tipo.

En Rust se hace una distinción entre expresión y sentencia. Los bloques, delimitados por llaves, pueden terminar por una expresión, que se evalúa a un valor concreto. En este caso, el bloque también será una expresión que devolverá ese valor:

\begin{figure}[H]
    \inputminted{rust}{code/functions.rs}
    \caption{La función \rust{plus_one} toma un parámetro y devuelve una expresión. El bloque dentro de \rust{let y} también se evalúa a una expresión.}
\end{figure}

\subsection{Control de flujo}
Además de las construcciones \rust{if}, \rust{if-else} y \rust{while}, Rust implementa el bucle infinito \rust{loop}, del que se puede salir con la sentencia \rust{break}. \rust{break} toma opcionalmente un valor que se asigna al bloque:

\begin{figure}[H]
    \inputminted{rust}{code/loop.rs}
    \caption{El resultado del bloque \rust{loop} se asigna a la variable mutable \rust{counter}}
\end{figure}

Por último, existe el bucle \rust{for} que, de forma parecida al de Python, recorre un iterador:

\begin{figure}[H]
    \inputminted{rust}{code/for.rs}
    \caption{\rust{(1..4)} va del 1 al 4 exclusive, y \rust{rev()} lo invierte}
\end{figure}

\section{Gestión segura de la memoria}
Los mecanismos que Rust utiliza para interactuar con la memoria es una de las grandes fortalezas del lenguaje. En esta sección introduciremos algunos de estos mecanismos, que se ilustrarán con ejemplos que demuestren su utilidad.

\subsection{Propiedad (\textit{ownership})}
La propiedad u \textit{ownership} es uno de los conceptos más característicos de Rust. Es la alternativa al recolector de basura de Java o el \texttt{malloc}/\texttt{free} de C para llevar a cabo una gestión segura de la memoria.

Ciertos tipos de datos, como \rust{String}, se almacenan en el montón (\textit{heap}), la zona de la memoria que se asigna dinámicamente. Para hacer un uso eficiente de la memoria, esta debe ser liberada cuando los datos dejan de ser válidos.

Rust hace esta gestión asignándole un propietario (\textit{owner}) a cada trozo de la memoria asignada, que será la variable que lo contiene. Cuando la variable deja de ser válida o, en otras palabras, el programa continúa la ejecución fuera del ámbito en el que reside, Rust llama automáticamente a la función \texttt{drop}, que libera la memoria.

\begin{figure}[H]
    \inputminted{rust}{code/scope.rs}
    \caption{Cuando la variable deja de ser válida, \textit{drop} hace su trabajo: libera la memoria asociada.}
\end{figure}

Cuando se llama a una función, la propiedad del valor que se le pasa como argumento se transfiere a la variable del argumento, que está en el ámbito de la función. Por ello, a partir del momento en el que una variable se pasa como argumento a una función, esta deja de ser válida en su ámbito inicial...

\begin{figure}[H]
    \inputminted{rust}{code/functionscope.rs}
    \caption{Tras llamar a \rust{takes_ownership(s)}, \rust{s} es liberada: ya no la podemos usar.}
\end{figure}

...a no ser que la función devuelva un valor. En ese caso, la propiedad es devuelta a la variable a la que se asigna la función.

\begin{figure}[H]
    \inputminted{rust}{code/functionscope2.rs}
    \caption{Estas funciones devuelven un valor, por tanto dejan de ser propietarias y el valor no es \rust{drop}eado}
\end{figure}

Para conservar el valor almacenado en el montón tras hacer una llamada a función, lo habitual en Rust es pasarle una \textbf{referencia} con el prefijo \rust{&}. Cuando pasamos una referencia como argumento de una función, el propietario se transfiere a la función, pero solo temporalmente. A esta acción Rust la llama \textbf{préstamo} (\textit{borrow}). Los préstamos no son liberados en memoria cuando las referencias se invalidan, sino que \textbf{se devuelven a su propietario original}. Si queremos poder modificar el valor en memoria, el préstamo debe ser mutable.

\begin{figure}[H]
    \inputminted{rust}{code/references.rs}
    \caption{Como le estamos pasando una referencia, Rust no libera la memoria asociada a \rust{s} hasta que no sale de \rust{main()}}
\end{figure}

\subsection{Algunos problemas clásicos}
A continuación, examinaremos varios problemas habituales en programación de sistemas relacionados con la gestión de la memoria y veremos cómo Rust los resuelve en el contexto de la propiedad y las referencias:

\subsubsection{Liberación doble (\textit{double free})}
Una liberación doble ocurre cuando se libera el mismo espacio de memoria dos veces. Esto causa un comportamiento indefinido, lo cual hace que nuestro programa se pueda comportar de forma impredecible, y pone en riesgo la seguridad de la memoria. En C ocurriría si hiciésemos \texttt{free()} dos veces pasándole el mismo puntero. En Rust ocurre cuando asignamos el valor de una variable de tipo dinámico a otra variable:

\begin{figure}[H]
    \inputminted{rust}{code/doublefree.rs}
\end{figure}

Si \rust{s1} y \rust{s2} apuntaran a la misma dirección de memoria, cuando las variables dejaran de ser válidas, se haría una liberación doble. Rust gestiona esto invalidando la variable \rust{s1}, y considera que \rust{s1} se ha movido a \rust{s2}. El \rust{println!}, por tanto, provocará un error de compilación para evitar una situación insegura.

\begin{figure}[H]
    \centering
    \includegraphics[width=200px]{safe_pointers.png}
\end{figure}

\subsubsection{Carrera de datos (\textit{data race})}
Una carrera de datos se da cuando las siguientes condiciones se cumplen:

\begin{itemize}
    \item Dos o más referencias acceden a la misma posición de memoria
    \item Al menos uno de las referencias se usa para escribir en memoria
    \item No hay un mecanismo que sincronice el acceso a la memoria
\end{itemize}

En Rust, esto ocurre cuando tenemos más de una referencia, y una de ellas es mutable. Para evitar este problema, Rust prohibe que haya más de una referencia si hay al menos una mutable. Sin embargo, se pueden tener tantas referencias inmutables como se quiera:

\begin{figure}[H]
    \inputminted{rust}{code/datarace.rs}
    \caption{Este código dará un error de compilación.}
\end{figure}

\subsubsection{Referencias colgantes (\textit{dangling pointers})}
El compilador vuelve a evitar otro problema típico gracias al mecanismo de propiedad. Ya que Rust sabe cuando una variable deja de estar en memoria por su ámbito, puede lanzar un error de compilación cuando tengamos una referencia a una variable cuyo valor ha sido borrado de la memoria:

\begin{figure}[H]
    \inputminted{rust}{code/dangle.rs}
    \caption{La variable \rust{s} se crea dentro de \rust{dangle()} y devolvemos una referencia a ella pero, al salir de la función, \rust{s} deja de ser válida y Rust hace un \rust{drop}: ¡referencia colgante! Este programa no compilará.}
\end{figure}

Otro problema parecido se da cuando tenemos una cadena (\rust{String}) y queremos obtener una subcadena que corresponda a la primera palabra. Una solución podría ser obtener el índice donde la primera palabra acaba:

\begin{figure}[H]
    \inputminted{rust}{code/slice1.rs}
\end{figure}

Pero cuando hacemos una llamada al método \rust{clear()}, la cadena se queda vacía. Nuestro índice es inválido, pero el programa puede continuar su ejecución. Rust resuelve esta situación mediante el tipo \textit{slice}: una referencia a un subconjunto de un tipo dinámico compuesto:

\begin{figure}[H]
    \inputminted{rust}{code/slice2.rs}
\end{figure}

Al usar un \textit{slice}, como en realidad es una referencia inmutable, este programa no compilará, ya que la regla de no tener más de un puntero si al menos uno es mutable se incumple. Hemos evitado un acceso potencial a una posición de memoria ya liberada:

\begin{figure}[H]
\begin{verbatim}
error[E0502]: cannot borrow `s` as mutable because it is also 
borrowed as immutable
  --> src/main.rs:18:5
   |
16 |     let word = first_word(&s);
   |                           -- immutable borrow occurs here
17 |
18 |     s.clear(); // error!
   |     ^^^^^^^^^ mutable borrow occurs here
19 |
20 |     println!("the first word is: {}", word);
   |                                       ---- 
   |           immutable borrow later used here
\end{verbatim}
\end{figure}

\subsection{Tiempos de vida (\textit{lifetimes})}

Una consecuencia del mecanismo de préstamos es que hay ciertas situaciones en las que Rust no puede inferir cuándo un préstamo se refiere a una variable válida. Observemos este caso: 

\begin{figure}[H]
    \inputminted{rust}{code/lifetime.rs}
\end{figure}

Rust no sabe si el \rust{&str} que devuelve la función tiene el mismo periodo de validez que el \rust{&str} de \rust{x} o el \rust{&str} de \rust{y} y, por tanto, no compila. Al estar utilizando referencias, podemos encontrarnos con varios inconvenientes. Consideremos el caso de un programa que imprime por pantalla la cadena más larga de entre dos cadenas:

\begin{figure}[H]
    \inputminted{rust}{code/lifetime2.rs}
\end{figure}

Podemos observar que el programa devolverá \rust{string1}, ya que es más larga, y de hecho está en el mismo ámbito que la llamada a \rust{println!}, pero Rust no sabe esto, y como \rust{string2} deja de ser válida cuando se hace la llamada a \rust{println!}, \rust{Rust} previene una posible referencia colgante.

Para gestionar esto, Rust implementa los \textbf{tiempos de vida} (\textit{lifetimes}), que son anotaciones que se hacen a las referencias para indicar el periodo de tiempo en el que permanecerán en memoria: 

\begin{figure}[H]
    \inputminted{rust}{code/lifetime3.rs}
\end{figure}

Nuestra renovada función \rust{longest} ahora incluye estas anotaciones, y Rust sabe que esta función se puede usar \textit{cuando tanto \rust{x} como \rust{y} tengan el mismo tiempo de vida}.

\subsection{Punteros inteligentes (\textit{smart pointers})}
Los punteros inteligentes son estructuras de datos que se comportan como punteros con algunas características adicionales. Normalmente son \texttt{struct}s, un tipo de dato compuesto que contiene otros datos. El concepto de \texttt{struct} es el mismo que en C. Suelen ser genéricos, y hay varios tipos.

\subsubsection{Rasgos (\textit{traits}), desreferenciación}

La particularidad que diferencia a un puntero inteligente de un \texttt{struct} es que implementa los rasgos \texttt{Deref} y \texttt{Drop} que la librería estándar de Rust provee. El \textbf{rasgo} (\textit{trait}) es un concepto similar a la interfaz en Java o a las clases de Haskell: define el comportamiento de una cierta clase de tipos de manera abstracta, y luego estos tipos implementan ese comportamiento genérico en el caso concreto, normalmente mediante métodos.

El rasgo \rust{Deref} permite a un puntero ser desreferenciado mediante la sintaxis \rust{*puntero}, que al programador experimentado de C le resultará familiar. Cuando desreferenciamos un puntero estamos manejando directamente el dato al que el puntero apunta en la memoria. \rust{Deref} obliga al tipo que lo implementa a definir el método \rust{deref()}, al que Rust llama automáticamente cuando utilizamos el operador de desreferenciación (\rust{*}).

Ejemplo: supongamos que tenemos el \rust{struct} genérico \rust{MyBox<T>} con un solo elemento de tipo T e implementamos el rasgo \rust{Deref}:

\begin{figure}[H]
    \inputminted{rust}{code/deref.rs}
    \caption{Implementando \rust{Deref} para nuestro tipo genérico \rust{MyBox<T>}}
\end{figure}

Ahora podemos desreferenciar \rust{MyBox}:

\begin{figure}[H]
    \inputminted{rust}{code/deref2.rs}
\end{figure}

Rust implementa la \textit{coerción de desreferenciación} implícita (\textit{deref coercion}): desreferencia automáticamente aquellos tipos que implementan \rust{Deref} cuando lo necesita. Es por esto que hay menos necesidad de utilizar el operador de desreferenciación que en otros lenguajes, como C:

\begin{figure}[H]
    \inputminted{rust}{code/derefcoercion.rs}
    \caption{Rust convierte nuestro \rust{&MyBox<String>} en \rust{&String} para poder pasárselo a la función \rust{hello()}}
\end{figure}

\subsubsection{El tipo Box}
\rust{Box<T>} es el puntero inteligente más básico de Rust. Nos sirve para almacenar información en el montón de la memoria en lugar de en la pila. Uno de sus usos más comunes es el de definir un tipo cuyo tamaño no puede ser definido en tiempo de compilación. Tomemos, por ejemplo, el caso de una \textit{lista cons}:

\begin{figure}[H]
    \inputminted{rust}{code/conslist.rs}
\end{figure}

La \textit{lista cons} (un concepto que viene de la función \texttt{cons} de Lisp) es una lista recursiva. Se contiene a sí misma, por lo que el compilador no sabe cuál es su tamaño en tiempo de compilación. Si lo hacemos con \rust{Box<T>}:

\begin{figure}[H]
    \inputminted{rust}{code/conslistbox.rs}
\end{figure}

Hemos ``roto'' la recursión: sabemos que \rust{Cons} siempre tendrá el tamaño de \rust{i32} más el de la información del puntero de \rust{Box<List>}, que en este caso es una dirección de memoria. Nil no almacena ningún valor, así que siempre ocupará menos.

Como es un puntero inteligente, \rust{Box<T>} implementa \rust{Deref} y \rust{Drop}. Así es como define su comportamiento como puntero. \rust{Drop} define el método \rust{drop()}, que define el comportamiento de las variables de un tipo genérico T cuando la ejecución del programa va a salir fuera de su ámbito. En \rust{Box<T>}, está implementado de manera que se libere el espacio que \rust{Box<T>} ocupa en memoria. No podemos llamar al método \rust{drop()} implementado por \rust{Drop} directamente, pero sí podemos llamar a \rust{std::mem::drop}, una función de la librería estándar de Rust que toma como argumento una variable y la libera. Como es de esperar, no tenemos que preocuparnos del posible \textit{double free}, ya que Rust se encarga de que la liberación solo ocurra una vez.

\section{Concurrencia sin miedo}
Rust llama al conjunto de herramientas que dan soporte a la programación en concurrencia y en paralelo ``concurrencia sin miedo'' (\textit{fearless programming}). Como en los otros casos en los que se trata con la memoria, Rust hace uso del compilador para imponer reglas estrictas que hagan que el código no lleve a situaciones problemáticas: condiciones de carrera, \textit{deadlocks}, etc.

Para la gestión de hilos, Rust utiliza un modelo 1:1 en el que un hilo en Rust corresponde a un hilo en el sistema operativo. El módulo \rust{std::thread} de la librería estándar provee una serie de métodos para manejarlos.

Los hilos se crean con \rust{spawn}. A \rust{spawn} se le pasa como argumento una \textbf{clausura}: una función anónima que es capaz de capturar su entorno (podemos utilizar variables que estén en el ámbito de su declaración) y que se puede pasar como argumento de otra función. \rust{spawn} devuelve un tipo \rust{JoinHandle} que podemos pasar como argumento a la función \rust{join}. El hilo que llama a \rust{join} es bloqueado hasta que el hilo asociado al \rust{JoinHandle} termina su ejecución:

\begin{figure}[H]
    \inputminted{rust}{code/threads.rs}
\end{figure}

Como hemos dicho, las clausuras pueden recibir como argumentos variables de su entorno. Con esto, Rust nos da una pista: ya que \rust{spawn} recibe una clausura, podemos compartir variables entre hilos. Sin embargo, Rust nos protege cuando las clausuras tratan de capturar variables desde otro hilo: la variable podría dejar de ser válida mientras se está ejecutando, así que obliga al hilo a obtener la propiedad de la misma mediante la palabra reservada \rust{move}. 

\begin{figure}[H]
    \inputminted{rust}{code/threads2.rs}
    \caption{Sin \rust{move}, este programa no compilaría}
\end{figure}

Los hilos son la unidad básica para programar con concurrencia en Rust. Sin embargo, son solo la punta del iceberg: Rust ofrece una variedad de mecanismos para modelar los distintas situaciones que implican el uso de hilos dependiendo de cómo queremos que se comuniquen y cómo queremos compartir la información entre ellos: canales de transmisión y recepción, acceso paralelo a memoria, etc.

\section{Conclusiones}
Desafortunadamente, los conceptos que caben en este documento son tan solo un pequeño subconjunto de todos los recursos que ofrece este lenguaje. Además de lo aquí expuesto, Rust ofrece una serie de recursos idiomáticos, paradigmas (en Rust también podemos pensar en objetos), abstracciones y herramientas que lo hace tan versátil como muchos de los lenguajes de programación más usados y, en muchos casos, más eficiente. 

Sin embargo, hemos querido transmitir una idea muy importante que prevalece en las distintas facetas de Rust: una de las mayores ventajas del lenguaje no es lo que puedes hacer con él, sino lo que no puedes hacer con él. Si Rust aspira a ser un lenguaje de sistemas moderno y fiable, debe hacer frente a los desafíos que la programación de sistemas presenta hoy en día y esto es precisamente lo que mejor hace: evitar los enormes desastres a los que una mala gestión de los recursos pueden llevar al desarrollador y obligarlo a escribir código más robusto, más mantenible y más seguro. 

\printbibliography

\end{document}
